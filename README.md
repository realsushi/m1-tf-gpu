# test m1 gpu with apple's metal api (proprietary)
## Abs:
why test gpu? Gpu compute is often very fast, but it is also harder to configure and debug. Indeed code must be interfaced with gpu libs. This is possible by compiling a library with a specific gpu target.

The good news is that Apple seems to be better on supporting data-science lib than Nvidia. (NV usually sticks with LTS - lon g term support - libs and distribs which is far from ideal).

## Tests:
The script would take a high level approach to detect the GPU. Note that we can detect GPU by importing metal api by linking `CoreGraphics.framework` then call `MTLCreateSystemDefaultDevice() `
### sources:
* [Apple's Metal API](https://developer.apple.com/documentation/metal)
* [reddit post](https://www.reddit.com/r/GraphicsProgramming/comments/uuiwd6/cli_apple_metal_m1_specific_equivalent_for/)
* [specific apple documentation](https://developer.apple.com/documentation/metal/1433401-mtlcreatesystemdefaultdevice)

## SETUP:
This is my current mood setup, completely fool proof (I 've not been able to trick myself yet!)
- miniforge / conda / miniconda
- pip for compatibility with docker (more on that topic later)
- python 3.10 (released 2 days ago with apple support, what a coincidence!)
- apple blob

## RUN:
```s
conda env create -f environment.yml
conda install -c apple tensorflow-deps
jupyter notebook --port=8888 
```
or run the python script 5 seconds to see if it segfaults

## Workflows
### Conda
Conda will serve as a wrapper of pip, this way our docker would use pip as usual but our environment would still be contained wihtin a conda env.  
*example*
```
cd 5.frontend
conda env create -f environment.yml
conda activate ai_nlp_travel
```
### missing a dep? 

run `conda install requests -c conda-forge`
conda is a pre-containerization step before packaging the app into a docker image (which don't require conda or moni-conda because... well it's a container!)

## Gitops features {Free Cookie}
- clean notebook : 
    - append to .git/config `[filter "remove-notebook-output"]
    clean = "jupyter nbconvert --clear-output --to=notebook --stdin --stdout --log-level=ERROR"`
    - create a gitattributes files with `*.ipynb clean = 1`

## TROUBLESHOOTING:
- numa warning: this is not a bummer, but it is a warning.
- minor issue: depending of the order of installation of packages, some libs are not at the latest version (potentially a conflict - read carefully the first lines when updating python env/ creating env)
- major issue: If the jupyter kernel dies (aka segfault),  a common good sense check is to note the gpu driver version. Generally speaking it's good to update the system in last resort (disable system update unless getting paid for fixing bugs).
Indeed the gpu driver version must somehow match the lib compiled version. As of now, TF 2.9 is rather recent so you may need a newer driver, thus an update.



## extra reading:
In case you want a future proof open source OS on your mac, check out asahi linux (https://asahi-linux.com/en/). - gpu not working at the moment and electron/chromium is not supported(but upstream is already patched...).

The asahi blog gives us tasty details on how m1 works and there is even an apple to apple comparison with other arm socs. Check also Alysa's blog (https://alyssa.ai/blog/2018/05/29/metal-1-on-macos/)
